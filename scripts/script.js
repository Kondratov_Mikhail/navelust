document.addEventListener('DOMContentLoaded', function() {
  const titles = document.querySelectorAll('.general__navigation-subtitle')
  const items = document.querySelectorAll('.general__navigation-item')

  for(let i = 0; i < items.length; i++) {
    titles[i].addEventListener('click', function() {
      const subitems = items[i].querySelectorAll('.general__navigation-subitem')
      for (let j = 0; j < subitems.length; j++) {
        if (subitems[j].style.display == 'none' || subitems[j].style.display == '') {
          subitems[j].style.display = 'block'
        } else {
          subitems[j].style.display = 'none'
        }
      }
    })
  }
}, false);
